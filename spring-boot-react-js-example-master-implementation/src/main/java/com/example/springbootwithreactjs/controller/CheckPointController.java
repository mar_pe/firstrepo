package com.example.springbootwithreactjs.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.springbootwithreactjs.model.CheckPoint;
import com.example.springbootwithreactjs.service.CheckPointService;

@RestController
@CrossOrigin
public class CheckPointController {
//Test
	@Autowired	
    private CheckPointService checkPointService;
	
	@GetMapping(path = "/checkpoint/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CheckPoint> getAllCheckPoints() {
		return checkPointService.getAllCheckPoints();  	
    }
	
	@PostMapping("/checkpoint/post")
    public void createCheckPoint(@RequestBody CheckPoint checkPoint) {
    	checkPointService.createCheckPoint(checkPoint);
    }
	
    @PutMapping("/checkpoint/update")
    public void updateCheckPoint(@RequestBody CheckPoint checkPoint) {
    	checkPointService.updateCheckPoint(checkPoint);
    }

    @DeleteMapping("/checkpoint/delete/{question}")
    public void deleteCheckPoint(@PathVariable String question) {
    	checkPointService.deleteCheckPoint(question);
    }	
}
