package com.example.springbootwithreactjs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.springbootwithreactjs.model.Todo;



@Repository
public interface TodoRepository extends JpaRepository<Todo, Integer>{

	
	 
}
