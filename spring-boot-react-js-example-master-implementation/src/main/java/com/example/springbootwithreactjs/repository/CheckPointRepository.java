package com.example.springbootwithreactjs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.springbootwithreactjs.model.CheckPoint;
import com.example.springbootwithreactjs.model.Todo;



@Repository
public interface CheckPointRepository extends JpaRepository<CheckPoint, Integer>{

	
	 
}
