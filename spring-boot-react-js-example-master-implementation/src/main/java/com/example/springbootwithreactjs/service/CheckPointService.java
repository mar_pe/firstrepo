package com.example.springbootwithreactjs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springbootwithreactjs.model.CheckPoint;
import com.example.springbootwithreactjs.repository.CheckPointRepository;



@Service
public class CheckPointService {

	@Autowired
	CheckPointRepository checkPointRepository;
	
	 public List<CheckPoint> getAllCheckPoints() {

		 List<CheckPoint> checkpointsList = checkPointRepository.findAll();
		 
	        return checkpointsList;
	    }

	public void updateCheckPoint(CheckPoint checkPoint) {
		
		
	}

	public void deleteCheckPoint(String question) {
		
		
	}

	public void createCheckPoint(CheckPoint checkPoint) {
		
	}
}
