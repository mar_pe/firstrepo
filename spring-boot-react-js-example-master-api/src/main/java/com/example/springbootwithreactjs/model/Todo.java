package com.example.springbootwithreactjs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name="todo")
@Getter
@Setter
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Todo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	@Column(name = "todo")
	private String todo;
	@Column(name = "todoStatus")
	private String todoStatus;

	public Todo(String todo, String todoStatus) {
		super();
		
		this.todo = todo;
		this.todoStatus = todoStatus;
	}

}
