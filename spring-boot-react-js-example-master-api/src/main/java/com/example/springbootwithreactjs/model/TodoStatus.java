package com.example.springbootwithreactjs.model;

public enum TodoStatus {

	BACKLOG, IN_PROGRESS, DONE;

}
