package com.example.springbootwithreactjs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="user")
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class User {
	
	
	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "userPassword")
	private String userPassword;


}
