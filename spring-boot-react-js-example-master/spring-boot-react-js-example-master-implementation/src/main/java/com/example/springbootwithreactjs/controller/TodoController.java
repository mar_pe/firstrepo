package com.example.springbootwithreactjs.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springbootwithreactjs.model.Todo;
import com.example.springbootwithreactjs.model.User;
import com.example.springbootwithreactjs.repository.TodoRepository;




@RestController
@CrossOrigin
public class TodoController {

	@Autowired	
    private TodoRepository todoRepository;
	
	final Logger logger = LogManager.getLogger(TodoController.class);
	
	
	@GetMapping(path = "/todo/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Todo> getAllTodos() {
		logger.info("@GetMapping aufgerufen um die Todos aus der Datenbank zu holen!");
		System.out.println("@GetMapping aufgerufen um die Todos aus der Datenbank zu holen!\"");
		return todoRepository.findAll();	
    }
	
	@GetMapping(path = "todo/get/{id}")
	public Todo getSingleTodo(@PathVariable Integer id) {
		logger.info("@GetMapping aufgerufen um Todo ein einzelnes Todo mit der {id} aus der Datenbank zu holen!");
		System.out.println("@GetMapping aufgerufen um Todo ein einzelnes Todo mit der {id} aus der Datenbank zu holen!");
		return todoRepository.findOne(id);
	}

	
	@PostMapping("/todo/post")
    public void createTodo(@RequestBody Todo todo) {
		System.out.println("@PostMapping aufgerufen um die Todos in die Datenbank zu speichern!\"");
    	todoRepository.save(todo);
    }
	
    @PutMapping("/todo/update/{id}")
    public void updateTodo(@PathVariable Integer id,@RequestBody Todo newTodo) {
    	
    	Todo oldTodo = this.getSingleTodo(id);
    	oldTodo.setId(id);
    	oldTodo.setTodo(newTodo.getTodo());
    	oldTodo.setTodoStatus(newTodo.getTodoStatus());
    	
    	System.out.println("@PutMapping aufgerufen ein Todo  zu verändern und wieder in die Datenbank zu speichern!");
    	todoRepository.save(oldTodo);
    }

    @DeleteMapping("/todo//delete/{id}")
    public void deleteTodo(@PathVariable Integer id) {
    	System.out.println("@DeleteMapping aufgerufen um die Todos aus der Datenbank zu löschen!\"");
    	todoRepository.delete(id);
    }

	
	
}
