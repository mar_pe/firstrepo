package com.example.springbootwithreactjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SpringBootApplication

public class SpringBootWithReactJsApplication {

	public static void main(String[] args) {
		final Logger logger = LogManager.getLogger(SpringBootWithReactJsApplication.class);
		SpringApplication.run(SpringBootWithReactJsApplication.class, args);
		System.out.println("Der Server läuft");
		logger.trace("Der Server läuft");
//		logger.debug("Debug Nachricht");
	
//		logger.warn("Warnung Nachricht");
//		logger.error("Error Nachricht");
//		logger.fatal("Kritische Nachricht");
	}
}
