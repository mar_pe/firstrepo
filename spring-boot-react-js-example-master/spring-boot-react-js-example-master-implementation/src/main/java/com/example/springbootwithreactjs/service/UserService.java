package com.example.springbootwithreactjs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springbootwithreactjs.model.User;
import com.example.springbootwithreactjs.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	
	public List<User> findAllUser() {
		
		return userRepository.findAll();
	}
	
	public User findOneUserById(Integer id) {
		return userRepository.findOne(id);
	}
	
	public User findUserByNameAndPassword(String name, String userPassword) {
		return userRepository.findByNameAndUserPassword(name, userPassword);
	}
	
	public void saveUser(User user) {
		userRepository.save(user);
	}
	
	public void updateUser(User user) {
		User oldUser = userRepository.findOne(user.getId());
		oldUser.setName(user.getName());
		oldUser.setUserPassword(user.getUserPassword());
		userRepository.save(oldUser);
	}
	
	public void deleteUser(Integer id) {
		userRepository.delete(id);
	}

}
