package com.example.springbootwithreactjs.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springbootwithreactjs.model.User;
import com.example.springbootwithreactjs.repository.UserRepository;
import com.example.springbootwithreactjs.service.UserService;

/**
 */
@RestController
@CrossOrigin
public class UserController {

	final Logger logger = LogManager.getLogger(UserController.class);
	
	@Autowired
	UserService userService;

	@GetMapping(path = "/user/get", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getUser() {
		return userService.findAllUser();
	}

	@GetMapping(path = "/user/get/{id}")
	public User getSingleUser(@PathVariable Integer id) {
		return userService.findOneUserById(id);		
	}
	@GetMapping(path = "/user/get/{name}/{userPassword}")
	public User getSingleUser(@PathVariable String name, @PathVariable String userPassword) {		
		return userService.findUserByNameAndPassword(name, userPassword);		
	}

	@PostMapping(path = "/user/post")
	public void postUser(@RequestBody User user) {
		userService.saveUser(user);
	}

	@PutMapping(path = "/user/put")
	public void putUser(@RequestBody User user) {		
		userService.updateUser(user);	
	}

	@DeleteMapping(path = "/delete/{id}")
	public void deleteUser(@PathVariable Integer id) {
		userService.deleteUser(id);
	}

}
